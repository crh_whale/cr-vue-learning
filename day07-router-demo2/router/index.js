import Vue from 'vue'
import VueRouter from 'vue-router'
import MyLogin from '@/components/MyLogin'
import MyHome from '@/components/MyHome'
import MyUsers from '@/components/menus/MyUsers'
import MyRights from '@/components/menus/MyRights'
import MyGoods from '@/components/menus/MyGoods'
import MyOrders from '@/components/menus/MyOrders'
import MySettings from '@/components/menus/MySettings'
import MyUserDetail from '@/components/user/MyUserDetail'

Vue.use(VueRouter)

const router = new VueRouter({
  routes: [
    {
      path: '/',
      redirect: '/login'
    },
    // 登录路由规则
    {
      path: '/login',
      component: MyLogin
    },
    {
      path: '/home',
      component: MyHome,
      redirect: '/home/users',
      children: [
        {
          path: 'users',
          component: MyUsers
        },
        {
          path: 'rights',
          component: MyRights
        },
        {
          path: 'goods',
          component: MyGoods
        },
        {
          path: 'orders',
          component: MyOrders
        },
        {
          path: 'settings',
          component: MySettings
        },
        //详情路由
        {
          path: 'detail/:id',
          component: MyUserDetail,
          props: true
        }
      ]
    }
  ]
})

// 路由全局前置守卫
router.beforeEach(function (to, from, next) {
  const pathArr = ['/home', '/home/users', '/home/rights','/home/orders']
  if (pathArr.indexOf(to.path) !== -1) {
    const token = localStorage.getItem('token')
    if (token) {
      next()
    } else {
      next('/login')
    }
  } else {
    next()
  }
})

export default router
