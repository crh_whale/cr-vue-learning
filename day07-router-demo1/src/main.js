import Vue from 'vue'
import App from './App2.vue'

// 导入 bootstrap 样式
import 'bootstrap/dist/css/bootstrap.min.css'
// 全局样式
import '@/assets/global.css'
// 导入路由模块
import router from '@/router'

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  // 在Vue项目中，要想把路由用起来 必须把路由实例对象 通过下面的方式进行挂载
  router: router
}).$mount('#app')
