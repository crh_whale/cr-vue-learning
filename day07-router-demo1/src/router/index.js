// src/router/index.js就是当前的路由模块

// 1.导入Vue和Router的包
import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/components/Home'
import Movie from '@/components/Movie'
import About from '@/components/About'
import Tab1 from '@/components/tabs/Tab1'
import Tab2 from '@/components/tabs/Tab2'
import Login from '@/components/Login'
import Main from '@/components/Main'

// 调用Vue.use()函数 把VueRouter 安装为 Vue 的插件
Vue.use(VueRouter)

// 3.创建路由的实例对象
const router = new VueRouter({
  // 数组:定义hash地址与组件之间的对应关系
  routes: [
    // 路由规则

    // 重定向的路由规则
    {
      path: '/',
      redirect: '/home'
    },
    {
      path: '/home',
      component: Home
    },
    {
      // movie组件中 根据id的值 ，展示对应电影的详情信息
      // 可以为路由规则开启props传参 从而方便的拿到动态参数
      path: '/movie/:id',
      component: Movie,
      props: true
    },
    {
      path: '/about',
      component: About,
      // 重定向
      redirect: 'about/tab1',
      // 通过children属性 嵌套声明子级路由规则
      // 如果children数组中 某个路由规则的path值为空字符串 则这条路由规则 叫做 默认子路由
      children: [
        {
          path: 'tab1',
          component: Tab1
        },
        {
          path: 'tab2',
          component: Tab2
        }
      ]
    },

    {
      path: '/login',
      component: Login
    },

    {
      path: '/main',
      component: Main
    }
  ]

})

// 为router实例对象 声明全局前置导航守卫
// 只要发生了路由的跳转 必然会触发beforeEach 指定的function回调函数
router.beforeEach(function (to, from, next) {
  // to 是将要访问的路由的信息对象
  // from 是将要离开的路由的信息对象
  // next 是一个函数 调用next() 表示放行 允许这次路由导航

  // 1 拿到用户将要访问的hash地址
  // 2 判断hash地址是否等于 /main (
  // 2.1 如果等于 /main 证明需要登录之后才能访问成功

  // 2.2 如果不等于 /main 则不需要登录 直接放行 next

  // 3 如果访问的地址是/main 则需要读取 localStorage中的token

  // 3.1 如果头token 则放行

  // 3.2 如果没有token 则强制跳转到 /login登录页

  if (to.path === '/main') {
    const token = localStorage.getItem('token')
    if (token) {
      next()
    } else {
      next('/login')
    }
  } else {
    next()
  }
})

// 4.向外共享路由的实例对象
export default router
