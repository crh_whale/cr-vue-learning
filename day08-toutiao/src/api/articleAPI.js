// 文章相关的API接口文档 都封装到这个模块中
// import request from '@/utils/request'

export const getArticleListAPI = function (_page, _limit) {
  // return request.get('/articles', {
  //   params: {
  //     _page: _page,
  //     _limit: _limit
  //   }
  // })
  const datalist = [
    {
      art_id: '8163',
      title: 'iOS原生混合RN开发最佳实践',
      aut_id: '1111',
      comm_count: '254',
      pubdate: '2019-03-11 09:00:00',
      aut_name: '黑马先锋',
      is_top: 0,
      cover: {
        type: 3,
        images: [
          'http://www.liulongbin.top:8000/resources/images/32.jpg',
          'http://www.liulongbin.top:8000/resources/images/80.jpg',
          'http://www.liulongbin.top:8000/resources/images/32.jpg'
        ]
      }
    },
    {
      art_id: '8089',
      title: 'Typescript玩转设计模式 之 创建型模式',
      aut_id: '1111',
      comm_count: '24',
      pubdate: '2019-03-11 09:00:00',
      aut_name: '黑马先锋',
      is_top: 0,
      cover: {
        type: 1,
        images: [
          'http://www.liulongbin.top:8000/resources/images/11.jpg'
        ]
      }
    },
    {
      art_id: '8145',
      title: 'JAVA消息确认机制之ACK模式',
      aut_id: '1111',
      comm_count: '99',
      pubdate: '2019-03-11 09:00:00',
      aut_name: '黑马先锋',
      is_top: 0,
      cover: {
        type: 0
      }
    },
    {
      art_id: '1111',
      title: 'JAVA原理技术',
      aut_id: '1111',
      comm_count: '111',
      pubdate: '2021-06-11 09:00:00',
      aut_name: '黑马先锋',
      is_top: 0,
      cover: {
        type: 0
      }
    }
  ]
  return datalist
}
