import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Vant from 'vant'
// import 'vant/lib/index.css'
// 切记 为了覆盖默认的less变量 一定要把后缀改成 .less
import 'vant/lib/index.less'

Vue.use(Vant)

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
