// 使用es6导入语法 导入jQuery
import $ from 'jquery'
// 导入样式(在webpack中，一切皆模块，都可以通过es6导入语法进行导入与使用)
// 如果某个模块中，使用from接收到的成员味undefined,则没必要进行接收
import './css/index.less'

// 导入图片，得到图片文件
import logo from './image/logo.png'
// 给img标签动态赋值
$('.box').attr('src',logo)

// 定义jQuery的入口函数
$(function () {
    // 实现奇偶行的变色效果
    // 奇数行
    $('li:odd').css('background-color','red')
    // 偶数行
    $('li:even').css('background-color','green')
})

// 定义一个装饰器函数
function info(target) {
    target.info = 'Person info.'
}
// 普通类
@info
class Person {}

console.log(Person.info)
