const path = require('path')
// 导入html-webpack-plugin插件 得到一个构造函数
const HtmlPlugin = require('html-webpack-plugin')

const {CleanWebpackPlugin} = require('clean-webpack-plugin')

// new构造函数创建html插件的实例对象
const htmlPlugin = new HtmlPlugin({
    // 指定原文件的存放路径(要复制的页面)
    template: './src/index.html',
    // 指定生成的文件的存放路径(复制出来的文件名和存放路径)
    filename: './index.html',
})

const cleanWebpackPlugin = new CleanWebpackPlugin()


// 使用Node.js中的导出语法 向外导出一个 webpack的配置对象
module.exports = {
    // 代表webpack运行的模式，可选值两个development(开发阶段) 和 production(上线)
    mode: 'development',
    // entry:'指定要处理哪个文件'
    entry: path.join(__dirname, './src/index1.js'),
    // 指定生成的文件要存放到哪里
    output: {
        // 存放的目录
        path: path.join(__dirname, 'dist'),
        // 生成的文件名
        filename: "js/bundle.js"
    },

    // 插件的数组，将来webpack在运行时，会加载并调用这些插件
    plugins: [htmlPlugin, cleanWebpackPlugin],
    devServer: {
        // 首次打包成功后，自动打开浏览器
        open: true,
        // 在http协议中，如果端口号是80 则可以被省略
        port: 80,
        // 指定运行的主机地址
        host: '127.0.0.1'
    },
    // 1.webpack 默认只能打包处理 .js 结尾的文件，处理不了其它后缀的文件
    //2.由于代码中包含了 index.css 这个文件，因此 webpack 默认处理不了
    //3.当 webpack 发现某个文件处理不了的时候，会查找 webpack.config.is 这个配置文件看 module.rules 数组中，是否配置了对应的 loader 加载器
    //4. webpack把index.css这个文件先转交给最后一个 loader 进行处理(先转交给 css-loader)
    //5.当css-loader处理完毕之后，会把处理的结果，转交给下一个loader(转交给 style-loader)
    //6.当style-loader处理完毕之后，发现没有下一个loader了，于是就把处理的结果，转交给了webpack
    // 7.webpack 把style-loader处理的结果，合并到 /dist/bundle.js 中，最终生成打包好的文件。
    module: {
        rules: [
            // 定义了不同模块对应的loader
            {test: /\.css$/, use: ['style-loader', 'css-loader']},

            // 处理.less文件的loader
            {test: /\.less$/, use: ['style-loader', 'css-loader', 'less-loader']},

            // 处理图片文件的loader
            // 如果需要调用的loader只有一个，则只传递一个字符串也行，如果多个loader,则必须指定数组
            // 在配置url-loader的时候，多个参数之间，使用&符号进行分隔
            {test: /\.jpg|png|gif$/, use: 'url-loader?limit=470&outputPath=images'},

            // 使用babel-loader处理高级的js语法
            // 在配置babel-loader的时候，只需要将自己代码进行转换，一定要排除node_modules目录中的js文件
            // 第三方包中的js文件兼容性，不需要考虑
            {test: /\.js$/, use: 'babel-loader', exclude: /node_modules/}
        ]
    },
    // 开发调试阶段 devtool值设置为eval-source-map
    // devtool: 'eval-source-map'
    // 实际发布,devtool的值设置为nosources-source-map或者直接关闭SourceMap
    devtool: 'nosources-source-map',


    resolve: {
        alias: {
            // 告诉webpack @符号表示src这一层目录
            '@': path.join(__dirname, './src')
        }
    }
}


