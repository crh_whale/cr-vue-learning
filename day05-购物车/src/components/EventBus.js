import Vue from 'vue'

export default new Vue()

// EventBus 的使用步骤
// 1 创建 eventBus.js 模块，并向外共享一个 Vue 的实例对象
// 2 在数据发送方，调用 bus.$emit('事件名称', 要发送的数据) 方法触发自定义事件
// 3 在数据接收方，调用 bus.$on('事件名称', 事件处理函数) 方法注册一个自定义事件
